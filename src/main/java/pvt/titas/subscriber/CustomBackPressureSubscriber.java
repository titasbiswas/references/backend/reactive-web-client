package pvt.titas.subscriber;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;

@Slf4j
public class CustomBackPressureSubscriber<T> extends BaseSubscriber<T> {
    int consumed, total;
    int batchSize, maxLimit;

    public CustomBackPressureSubscriber(int batchSize, int thresold) {
        this.batchSize = batchSize;
        this.maxLimit = thresold;
    }

    @Override
    protected void hookOnSubscribe(Subscription subscription) {
        subscription.request(batchSize);
    }

    @Override
    protected void hookOnNext(T value) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("--- Item consumed---" + value);
        consumed++;
        total++;
        if (consumed == batchSize) {
            log.info("--- Batch Completed---");
            consumed = 0;
            request(batchSize);
        }
        if (total == maxLimit)
            cancel();
    }
}
