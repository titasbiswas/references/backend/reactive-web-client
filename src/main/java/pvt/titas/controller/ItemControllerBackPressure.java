package pvt.titas.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import pvt.titas.domain.Item;
import pvt.titas.subscriber.CustomBackPressureSubscriber;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping
@Slf4j
public class ItemControllerBackPressure {

    private WebClient webClient = WebClient.create("http://localhost:8080");

    @GetMapping("/item/client/bp")
    public void getAllItemBackPressure() {
        CustomBackPressureSubscriber<Item> itemBackpressureSubscriber = new CustomBackPressureSubscriber<>(2, 5);

        Flux<Item> itemFlux=  webClient.get()
                .uri("/v1/items")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(Item.class);

        itemFlux.subscribe(itemBackpressureSubscriber);
    }
}
