package pvt.titas.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import pvt.titas.domain.Item;
import pvt.titas.subscriber.CustomBackPressureSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/item/client")
@Slf4j
public class ItemClientController {


    private WebClient webClient = WebClient.create("http://localhost:8080");

    @GetMapping
    public Flux<Item> getAllItemRetrieve() {
        return webClient.get()
                .uri("/v1/items")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(Item.class);
    }


    @GetMapping("/exchange")
    public Flux<Item> getAllItemExchange() {
        return webClient.get()
                .uri("/v1/items")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMapMany(response -> response.bodyToFlux(Item.class));
    }

    @GetMapping("/{id}")
    public Mono<Item> getOneItem(@PathVariable String id) {
        return webClient.get()
                .uri("/v1/items/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Item.class);
    }

    @GetMapping("/error/{id}")
    public Mono<Item> getOneItemError(@PathVariable String id) {
        return webClient.get()
                .uri("/v1/items/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, respoponse->{
                    return respoponse.bodyToMono(String.class).flatMap(error ->{
                        log.error("Error in web client", error);
                        throw new RuntimeException(error);
                    });
                })
                .bodyToMono(Item.class);
    }

    @GetMapping("/error/exchange/{id}")
    public Mono<Item> getOneItemError_exchange(@PathVariable String id) {
        return webClient.get()
                .uri("/v1/items/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(response ->{
                    if(response.statusCode().is5xxServerError()){
                        return response.bodyToMono(String.class).flatMap(e->{
                           log.error("Error in exchange:", e);
                           throw new RuntimeException(e);
                        });
                    }
                    else if(response.statusCode().is4xxClientError()){
                        return response.bodyToMono(String.class).flatMap(e->{
                            log.error("Request error", e);
                            throw new RuntimeException(e);
                        });
                    }
                    else {
                        return response.bodyToMono(Item.class);
                    }
                });
    }

    @PostMapping
    public Mono<Item> updateItem(@RequestBody Item item) {
        return webClient.post()
                .uri("/v1/items/")
                .body(BodyInserters.fromValue(item))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Item.class);
    }


    @PutMapping("/{id}")
    public Mono<Item> updateItem(@PathVariable String id,
                                 @RequestBody Item item) {
        return webClient.put()
                .uri("/v1/items/{id}", id)
                .body(BodyInserters.fromValue(item))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Item.class);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteItem(@PathVariable String id) {
        return webClient.delete()
                .uri("/v1/items/{id}", id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Void.class);
    }
}
